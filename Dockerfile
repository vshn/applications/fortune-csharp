# Stage 1: builder image
FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /build
COPY fortune-dotnet.csproj .
RUN dotnet restore
COPY Controllers /build/Controllers
COPY Models /build/Models
COPY Properties /build/Properties
COPY Views /build/Views
COPY wwwroot /build/wwwroot
COPY Program.cs /build
RUN dotnet publish --configuration release --output ./out --no-restore

# tag::production[]
# Stage 2: runtime image
FROM mcr.microsoft.com/dotnet/aspnet:7.0-alpine
RUN apk add fortune
WORKDIR /dotnetapp
COPY --from=build /build/out .
COPY appsettings.json /dotnetapp

EXPOSE 8080

# <1>
USER 1001

ENTRYPOINT ["dotnet", "fortune-dotnet.dll"]
# end::production[]
