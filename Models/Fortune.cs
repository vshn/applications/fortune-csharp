namespace fortune_dotnet.Models;

public record Fortune(int Number, string Message, string Version, string Hostname);
