using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Net;
using System.Diagnostics;
using fortune_dotnet.Models;

namespace fortune_dotnet.Controllers;

[ApiController]
[Route("/")]
public class FortuneController : Controller
{
    private readonly ILogger<FortuneController> _logger;

    public FortuneController(ILogger<FortuneController> logger)
    {
        _logger = logger;
    }

    private Fortune MakeFortune()
    {
        var result = GetFortuneMessage();
        var random = GetFortuneNumber();
        var hostname = GetHostName();
        return new Fortune(random, result, "1.2-c#", hostname);
    }

    // tag::router[]
    [HttpGet]
    public IActionResult GetFortune()
    {
        var fortune = MakeFortune();
        var acceptHeader = Request.Headers.ContainsKey("Accept") ? Request.Headers["Accept"].ToString() : "text/html";
        if (acceptHeader == MediaTypeNames.Application.Json)
        {
            return Ok(fortune);
        }
        else if (acceptHeader == MediaTypeNames.Text.Plain)
        {
            return Content($@"Fortune {fortune.Version} cookie of the day #{fortune.Number}:

{fortune.Message}
Pod: {fortune.Hostname}", "text/plain");
        }
        else
        {
            return View("index", fortune);
        }
    }
    // end::router[]

    private string GetFortuneMessage()
    {
        var process = new Process()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "/bin/sh",
                Arguments = $"-c fortune",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            }
        };

        process.Start();
        string result = process.StandardOutput.ReadToEnd();
        process.WaitForExit();

        return result;
    }

    private int GetFortuneNumber()
    {
        var random = new Random();
        return random.Next(1000);
    }

    private string GetHostName()
    {
        return Dns.GetHostName();
    }
}
